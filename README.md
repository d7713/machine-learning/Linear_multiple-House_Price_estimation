# House Price Estimation (Different Linear models)

### Models Used:
**Polynomial Regression, Quantile Regression, Ridge Regression, Lasso Regression, Elastic net Regression**

### By: Andrew Wairegi

## Description
To analyse house pricing data, to be able to find the main predictors of house prices. 
As well as create a model that will allow me to predict house prices, using different linear models
to improve the model. This will allow the Hass Consulting company, to determine the house prices for their houses. 
As well as evaluate the prices of other house sellers. Without the need for a physical realtor. This will allow them to do
their house evaluations quickly. As well as cut costs. As not many house realtors would be needed.

[Open notebook]

## Setup/installation instructions
1. Find a local folder on your computer
2. Setup it up as a empty repository (using git init)
3. Clone this repository there, into the local folder (git clone https://...)
4. Upload the collaboratory notebook to google drive
5. Open it online
6. Upload the data files to the google collab (in the file upload section)
7. Run the notebook

## Known Bugs
There are no known issues

## Technologies Used
1. Python - The programming language
2. Numpy - An Arithmetic Package
3. Pandas - A Data Analysis Package
4. Seaborn - A Visualization package
5. Matplotlib - A Visualization package
6. Scikit learn - A Modelling package
7. Statsmodels - A Statistical Modelling package
8. Scipy - A Statistics package

<br>

### License
Copyright © 2021 **Andrew Wairegi**
<br>
You are free to view, but not copy.

[Open notebook]: https://gitlab.com/d7713/machine-learning/Linear_multiple-House_Price_estimation/-/blob/main/House_Price_Estimation_Hass_Consulting_(linear_models).ipynb
